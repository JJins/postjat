// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
import { app, BrowserWindow, ipcMain, webContents } from 'electron'
import { join } from 'path'
import { electronApp, optimizer, is } from '@electron-toolkit/utils'
import icon from '../../resources/icon.png?asset'

let mainWin: BrowserWindow

function createWindow(): void {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    show: false,
    autoHideMenuBar: true,
    ...(process.platform === 'linux' ? { icon } : {}),
    webPreferences: {
      preload: join(__dirname, '../preload/index.js'),
      sandbox: false,
      webviewTag: true,
      nodeIntegration: true
    }
  })
  mainWin = mainWindow

  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
  })

  // mainWindow.webContents.setWindowOpenHandler((details) => {
  //   shell.openExternal(details.url)
  //   return { action: 'deny' }
  // })

  // CSP policy
  // const cspHeader = "default-src 'self' data:; img-src 'self' data:;"
  // mainWindow.webContents.session.webRequest.onHeadersReceived((details, callback) => {
  //   details.responseHeaders = details.responseHeaders ? details.responseHeaders : {}
  //   if (details.responseHeaders['X-Frame-Options']) {
  //     delete details.responseHeaders['X-Frame-Options']
  //   } else if (details.responseHeaders['x-frame-options']) {
  //     delete details.responseHeaders['x-frame-options']
  //   }
  //   details.responseHeaders['Content-Security-Policy'] = cspHeader
  //   callback({ cancel: false, responseHeaders: details.responseHeaders })
  // })

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
  if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
    mainWindow.loadURL(process.env['ELECTRON_RENDERER_URL'])
  } else {
    mainWindow.loadFile(join(__dirname, '../renderer/index.html'))
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  // Set app user model id for windows
  electronApp.setAppUserModelId('com.electron')

  // Default open or close DevTools by F12 in development
  // and ignore CommandOrControl + R in production.
  // see https://github.com/alex8088/electron-toolkit/tree/master/packages/utils
  app.on('browser-window-created', (_, window) => {
    optimizer.watchWindowShortcuts(window)
  })

  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

/**
 * 从renderer处获取webContents
 * @param webContentsId
 * @param contents
 */
const getGuestForWebContents = (webContentsId, contents) => {
  console.log('获取webcontentsid', { webContentsId, contents })
  const guest = webContents.fromId(webContentsId)
  if (!guest) {
    throw new Error(`Invalid webContentsId: ${webContentsId}`)
  }
  if (guest.hostWebContents !== contents) {
    throw new Error('Access denied to webContents')
  }
  return guest
}

ipcMain.handle('openDevTools', (event, webContentsId) => {
  const guest = getGuestForWebContents(webContentsId, event.sender)
  guest.openDevTools()
})

// do the fetch for renderer then return the plain text response
ipcMain.handle('requestInit', async (_, arg) => {
  console.log('Message from renderer process:', arg)
  let plainResponse: string | null = null
  try {
    plainResponse = await (await handleRequest(arg)).text()
  } catch (e) {
    console.error(e)
  }
  return { response: plainResponse, success: plainResponse !== null }
})

// https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch
const handleRequest = async (arg) => {
  const { method, url, heads, body } = arg
  return fetch(url, { method, headers: heads, body })
}
