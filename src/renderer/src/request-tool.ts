import { ipcRenderer } from './wrapper'

const RequestTool = {
  get: async (url: string, heads?: Headers) => {
    const result = await ipcRenderer.invoke('requestInit', { method: 'GET', url, heads })
    if (result.success) {
      return result.response
    } else {
      throw Error('Request failed.')
    }
  },
  post: async (url: string, body: unknown, heads?: Headers) => {
    const result = await ipcRenderer.invoke('requestInit', { method: 'POST', body, url, heads })
    if (result.success) {
      return result.response
    } else {
      throw Error('Request failed.')
    }
  },

  isValid: (url: string): boolean => {
    try {
      new URL(url)
      return true
    } catch (e: any) {
      console.warn('invalid url input: ', e.message)
      return false
    }
  }
}

export { RequestTool }
