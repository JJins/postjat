import { isRef, Ref } from 'vue'

const FormatTools = {
  json: (before: Ref<string> | string) => {
    let str: string
    if (isRef(before)) {
      str = before.value
    } else {
      str = before
    }
    // let after: string
    // try {
    const obj = JSON.parse(str)
    const after = JSON.stringify(obj, null, '  ')
    // } catch (e) {
    //   throw e
    // }
    if (isRef(before)) {
      before.value = after
    }
    return after
  }
}

export { FormatTools }
