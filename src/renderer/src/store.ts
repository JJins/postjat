import { reactive } from 'vue'

// TODO
const useRequestStore = () => {
  return reactive({
    inputUrl: '',
    requestMethod: 'GET',
    requestBody: '',
    requestBodyType: '',
    responseBody: ''
  })
}

const data = reactive({
  // request
  inputUrl: '',
  requestMethod: 'GET',
  requestBody: '',
  requestBodyType: '',
  responseBody: '',
  // ui
  panelSplitDirection: 'vertical'
})

export { useRequestStore }
export default data
