import { createApp } from 'vue'
import App from './App.vue'
import './monaco-workers'

const app = createApp(App)
app.mount('#app')
